from django.db import models


class TwitterUser(models.Model):

    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50, default='0')

    def __str__(self):
        return str(self.id)


class TwitterFollower(models.Model):

    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50, default='0')
    followed = models.ForeignKey(TwitterUser, on_delete=models.CASCADE, related_name='followed_user')
    followercount = models.IntegerField()

    def __str__(self):
        return str(self.id)