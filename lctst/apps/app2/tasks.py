from __future__ import absolute_import
from celery.decorators import periodic_task
from lctst.apps.app2.twitapi import tweetit, retweet
from datetime import timedelta


@periodic_task(run_every=timedelta(hours=12))
def retweet_periodic(screen_name):

    retweet(screen_name)


@periodic_task(run_every=timedelta(days=3))
def tweet_periodic():

    tweet_body = "Time for some Java livestreams: https://www.livecoding.tv/username1, https://www.livecoding.tv/username2"
    tweetit(tweet_body)