from django.views.generic.base import View, TemplateView
from lctst.apps.app2.models import TwitterUser, TwitterFollower
from lctst.apps.app2 import twitapi
from lctst.apps.app2.tasks import retweet_periodic

class JuliaView(TemplateView):

    template_name = "app2/julia.html"

    def get_context_data(self, **kwargs):

        context = super(JuliaView, self).get_context_data(**kwargs)

        return context


class UserFollowerCreateView(View):

    def post(self, request, *args, **kwargs):

        username = request.POST['username']

        u = TwitterUser(username=username)
        u.save()

        retweet_periodic.delay(username)

        followers = twitapi.followers(username)

        for follower, count in followers.items():
            f = TwitterFollower(username=follower, followercount=count)
            f.followed = TwitterUser.objects.get(username=username)
            f.save()