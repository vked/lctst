from django.contrib import admin
from lctst.apps.app2.models import TwitterUser, TwitterFollower


admin.site.register(TwitterUser)
admin.site.register(TwitterFollower)

