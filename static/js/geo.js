$( document ).ready(function() {

    $('#grant-permission').on('click', function() {

        function getLocation() {

            console.log('Hi');

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(saveLocation);
            } else {

            }

        }

        function saveLocation(position) {

            var myLatitude = position.coords.latitude.toString();
            var myLongitude = position.coords.longitude.toString();

            console.log(latitude);

            $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    csrftoken = getCookie('csrftoken');

                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                }
            });

            $.ajax({
                url: "/geo/create/detect/",
                type: "POST",
                data: { latitude: myLatitude,
                        longitude: myLongitude,
                        deneme: '1'
                },
                success : function(json) {
                    console.log('Created.');
                },
                error : function(xhr,errmsg,err) {
                    console.log(err);
                }
            });

        }

        getLocation();

    });

    $('#locate-city').on('click', function() {

        var myCity = $("#city-name").val()

        $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    csrftoken = getCookie('csrftoken');

                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                }
            });

        $.ajax({
            url: "/geo/create/city/",
            type: "POST",
            data: { getCity: myCity },
            success : function(json) {
                console.log(myCity);
            },
            error : function(xhr,errmsg,err) {
            }
        });

    });

    $('#locate-country').on('click', function() {

        var myCountry = $("#country-name").val()
        var myZip = $("#zip-code").val()

        $.ajaxSetup({
                beforeSend: function(xhr, settings) {
                    csrftoken = getCookie('csrftoken');

                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                }
            });

        $.ajax({
            url: "/geo/create/country/",
            type: "POST",
            data: { getCountry: myCountry,
                    getZip: myZip
            },
            success : function(json) {
                console.log('Created.');
            },
            error : function(xhr,errmsg,err) {
                console.log(err);
            }
        });

    });

});

function getCookie(name) {

    var cookieValue = null;

    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }

    return cookieValue;

}

function csrfSafeMethod(method) {

    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));

}